# IIS & PHP #

Base box intended to setup a functioning Windows Server Box with PHP & SQL Server ready to roll...

## Includes: ##
* Windows Server 2008 R2
* SQL Server 2008
* IIS 7.5
* PHP 5.4

## Dependencies: ##
This machine will install web platform installer and sql server. The installers should be placed in the root of this repo, but do currently need to be downloaded manually.

* SQL Server - http://download.microsoft.com/download/0/4/B/04BE03CD-EAF3-4797-9D8D-2E08E316C998/SQLEXPRWT_x64_ENU.exe
* Web Platform Installer - http://download.microsoft.com/download/C/F/F/CFF3A0B8-99D4-41A2-AE1A-496C08BEB904/WebPlatformInstaller_amd64_en-US.msi

Doing a vagrant up without these files will also provide you these links.

## Useful links: ##
* https://github.com/fgrehm/vagrant-mssql-express
* http://kwilson.me.uk/blog/provisioning-a-windows-server-vagrant-box-with-iis-net-4-5-and-octopus-deploy/
* http://www.iis.net/learn/install/web-platform-installer/web-platform-installer-v4-command-line-webpicmdexe-rtw-release
* http://forums.iis.net/t/1199188.aspx?Silent+installation+of+WebMatrix
* http://www.ca.com/us/support/ca-support-online/product-content/knowledgebase-articles/tec1193892.aspx
* https://vagrantcloud.com/opentable/
* https://github.com/da5is/powershell-php-install
* http://stackoverflow.com/questions/22636106/iis-application-using-shared-folder-in-virtualbox-vm
* https://github.com/mefellows/vagrant-smb-plugin