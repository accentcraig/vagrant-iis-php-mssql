$ErrorActionPreference = "Stop"
echo "Creating IIS website..."
 
Import-Module WebAdministration

[Environment]::SetEnvironmentVariable("ACC_DEV", "true", "Machine")

Get-ChildItem -Path iis:\Sites | foreach { Stop-WebSite $_.Name; }
If (Test-Path iis:\Sites\vagrant) {
	Remove-Item iis:\Sites\vagrant
}
New-Item iis:\Sites\vagrant -id 1 -bindings @{protocol="http";bindingInformation="*:80:"} -physicalPath c:\vagrant-local\src
Start-Website -name "vagrant"
 
echo "Created!"